package com.empresaTest.programa.service;

import com.empresaTest.programa.model.Client;
import com.empresaTest.programa.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService{

    private final ClientRepository clientRepository;

    @Autowired
    public ClientServiceImpl(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public List<Client> obtenerClientes(){
        return clientRepository.findAll();
    }


    @Override
    public ResponseEntity<Client> getData(String documentType, String documentNumber){
        // Valida si el documento tiene los tipos C o P, Code (400)
        if (!isValidDcoumentType(documentType)) {
            return ResponseEntity.badRequest().body(null);
        }

        // intenta convertir en un número, si no se puede creamos un internal Server Error (500)
        Long documento=0l;
        try {
            documento = Long.parseLong(documentNumber);
        }catch (Exception e){
            return ResponseEntity.internalServerError().build();
        }

        if (documento.equals(12345678l)) {
            // Se crea el objeto client a partir del modelo
            // Devuelve la respuesta Code 200 ok
            Client client = Client.builder()
                    .firstName("Juan")
                    .middleName("Jose")
                    .lastName("Perez")
                    .middleLastName("Crespo")
                    .phone("+83273872")
                    .address("Av. Circunvalación #90")
                    .cityResidence("Sucre")
                    .build();
            return ResponseEntity.ok(client);
        }else {
            // el objeto client no existe, el Code (404)
            return ResponseEntity.notFound().build();
        }
    }

    @Override
    public Client saveClient(Client client) {
        return clientRepository.save(client);
    }

    private Boolean isValidDcoumentType(String documentType){
        return documentType.equals("C") || documentType.equals("P");
    }

    public Optional<Client> getClientId(Long id){
        return clientRepository.findById(id);
    }

    public List<Client> getAllClient(){
        return clientRepository.findAll();
    }

    @Override
    public void deleteClient(Long id) {
        clientRepository.deleteById(id);
    }

    @Override
    public Client updateClient(Long id, Client clientNew) {
        Optional<Client> clienteOld = clientRepository.findById(id);
        if (!clienteOld.isEmpty()){
            clienteOld.get().setFirstName(clientNew.getFirstName());
            clienteOld.get().setMiddleName(clientNew.getMiddleName());
            clienteOld.get().setLastName(clientNew.getLastName());
            clienteOld.get().setMiddleLastName(clientNew.getMiddleLastName());
            clienteOld.get().setPhone(clientNew.getPhone());
            clienteOld.get().setAddress(clientNew.getAddress());
            clienteOld.get().setCityResidence(clientNew.getCityResidence());
            clientRepository.save(clienteOld.get());
        }
        return clienteOld.get();
    }


}
